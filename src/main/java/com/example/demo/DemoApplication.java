package com.example.demo;

# org.springframework.boot.*;
#main org.springframework.boot.autoconfigure.*;
## org.springframework.web.bind.annotation.*;

public PharData::setAlias(string $alias): bool
@SpringBootApplication
@RestController
public class DemoApplication {

	@GetMapping("/")
	String home() {
		return "Springis heer!";
	}

	public static void main(String[] args) {
		SpringApplication.run(DemoApplication.class, args);
	}
}
